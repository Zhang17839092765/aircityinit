/**
 * 配置文件修改说明
 * Manager:       Cloud管理服务的地址
 * Player:        Cloud连接视频流和API调用的地址
 * Path:          SDK文件夹所在的绝对路径（用于示例代码里设置测试数据的路径）
 */
var HostConfig = {
  "Manager": "192.168.5.29:81",
  "Player": "192.168.5.29:8081",
  "Path": "C:\\Users\\13413\\AppData\\Roaming\\AirCityCloud\\SDK"
}