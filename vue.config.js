/*
 * @Author: your name
 * @Date: 2021-08-02 00:14:12
 * @LastEditTime: 2021-10-09 23:38:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\vue.config.js
 */
const path = require("path");

const title = "初始化项目";
module.exports = {
  publicPath: "./",
  outputDir: process.env.NODE_ENV === "development" ? "devdist" : "proddist", // 不同的环境打不同包名
  // outputDir: 'dist',
  lintOnSave: false, // 关闭eslint
  productionSourceMap: true, // 生产环境下css 分离文件
  devServer: {
    // 配置服务器
    open: true,
    // 项目运行时候的端口号
    port: 3000,
    // https: false,
    disableHostCheck: true,

    overlay: {
      warnings: true,
      errors: true,
    },
    proxy: {
      "/api": {
        target: "http://120.77.174.86:8200/",
        changeOrigin: true,
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
  css: {
    sourceMap: true, // 开启 CSS source maps
    loaderOptions: {
      sass: {
        prependData: "@import '@/styles/common.scss';",
      },
    },
  },

  configureWebpack: {
    resolve: {
      // 配置解析别名
      alias: {
        "@": path.resolve(__dirname, "./src"),
        "@s": path.resolve(__dirname, "./src/assets/style"),
        "@i": path.resolve(__dirname, "./src/assets/images"),
      },
    },
  },
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = title;
      return args;
    });
  },
};
