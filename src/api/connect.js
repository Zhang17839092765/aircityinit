/*
 * @Author: your name
 * @Date: 2021-08-02 00:32:54
 * @LastEditTime: 2021-10-23 12:36:36
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\api\home.js
 */
import axios from "../utils/HTTP";
import { Service, Mock } from "../utils/service"; // 把请求地址模块化统一管理

export const getMockData = () => {
  return axios.get(Mock.aircity + "/test.json");
};
// 请求接口示例
export const getApiData = () => {
  return axios.get(Service.mdoel1 + "/test");
};
