/*
 * @Author: your name
 * @Date: 2021-08-02 10:58:34
 * @LastEditTime: 2021-08-02 10:59:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\tools\is.js
 */
//判断系统类型
const OSnow = () => {
    var agent = navigator.userAgent.toLowerCase()
    var isMac = /macintosh|mac os x/i.test(navigator.userAgent)
    if (agent.indexOf('win32') >= 0 || agent.indexOf('wow32') >= 0) {
        //your code
        alert('这是windows32位系统')
    }
    if (agent.indexOf('win64') >= 0 || agent.indexOf('wow64') >= 0) {
        //your code
        alert('这是windows64位系统')
    }
    if (isMac) {
        //your code
        alert('这是mac系统')
    }
}
export { OSnow }