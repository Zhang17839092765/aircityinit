/*
 * @Author: your name
 * @Date: 2021-08-02 00:14:12
 * @LastEditTime: 2021-09-07 09:39:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\router\index.js
 */
import Vue from "vue";
import VueRouter from "vue-router";
// 引入路由模块
import { TagPageRoute } from "./modules/tagpages";
import { AdministrationRoute } from "./modules/administration";
Vue.use(VueRouter);
// 获取原型对象上的push函数
const originalPush = VueRouter.prototype.push;
// 修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
const routes = [
  {
    path: "/",
    redirect: "home",
  },
  {
    path: "/home",
    name: "home",
    component: () =>
      import(/* webpackChunkName: "home" */ "@/views/Home/Home.vue"),
  },
  // 在此插入路由模块
  TagPageRoute,
  AdministrationRoute,
  {
    path: "/404",
    name: "NotFound",
    component: () =>
      import(/* webpackChunkName: "notFound" */ "@/views/404/index.vue"),
  },
  {
    path: "*", //处理错误页面
    redirect: "/404",
  },
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
});
// 添加路由守卫
// router.beforeEach((to, from, next) => {
//   // 可以添加登录前的导航守卫

//   console.log('to:', to);
//   console.log('from:', from);
//   next()
// })
export default router;
