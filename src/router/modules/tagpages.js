/*
 * @Author: your name
 * @Date: 2021-08-02 00:55:39
 * @LastEditTime: 2021-08-02 01:19:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\router\modules\tagpages.js
 */
// 标签的url使用路由
export const TagPageRoute = {
  path: "/tagpage",
  name: "Tagpage",

  component: () => import("@/components/tagpages"),
  children: [
    {
      path: "model1",
      name: "Model1",
      component: () =>
        import("@/components/tagpages/components/TagePageModel1"),
    },
    {
      path: "model2",
      name: "Model2",
      component: () =>
        import("@/components/tagpages/components/TagePageModel2"),
    },
  ],
};
