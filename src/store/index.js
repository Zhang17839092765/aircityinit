/*
 * @Author: your name
 * @Date: 2021-08-02 00:14:12
 * @LastEditTime: 2021-08-02 03:20:07
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\store\index.js
 */
import Vue from "vue";
import Vuex from "vuex";

// 导入模块
import { AirCity } from "./modules/AirCity"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
    },
    mutations: {

    },
    actions: {

    },
    modules: {
        AirCity
    }
});
