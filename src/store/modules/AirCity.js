// AirCity 模块状态管理
const AirCity = {
    namespaced: true, //定义module另外命名时，需要在module中加一个命名空间namespaced: true属性，否则命名无法暴露出来，导致报[vuex] module namespace not found in mapState()等错误。
    state: {
        AirCityPlayer: null,
        AirCityApi: null

    },
    mutations: {
        setAirCityPlayer_mut(state, val) {
            state.AirCityPlayer = val
        },
        setAirCityApi_mut(state, val) {

            state.AirCityApi = val
        }
    },
    actions: {
        setAirCityPlayer_act({ commit }, val) {
            commit('setAirCityPlayer_mut', val)
        },
        setAirCityApi_act({ commit }, val) {

            commit('setAirCityApi_mut', val)
        }
    }
}

export { AirCity }

