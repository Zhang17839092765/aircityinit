/*
 * @Author: your name
 * @Date: 2021-08-02 00:14:12
 * @LastEditTime: 2021-08-10 12:55:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
Vue.use(Antd);
Vue.use(ElementUI);
// 全局样式
import "./assets/css/global.scss";
import "./assets/css/rest.css";
Vue.config.productionTip = false
// window.location.origin兼容问题
if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
}
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
