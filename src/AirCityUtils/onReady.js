/*
 * @Author: your name
 * @Date: 2021-08-30 15:24:44
 * @LastEditTime: 2021-10-09 15:57:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \aircityinit\src\AirCityUtils\onReady.js
 */
const OnReady = async () => {
  __g.camera.get((r) => {
    console.log(`Camera: ${r.x}, ${r.y}, ${r.z}, ${r.pitch}, ${r.yaw}`);
  });
};
export default OnReady;
